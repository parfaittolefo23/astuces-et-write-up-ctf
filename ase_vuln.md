# AES 128 Padding Attack

<details><summary>AES 128 Padding Attack</summary>

Un challeneg: [AES-128-padding-attack](https://godiego.co/posts/AES-128-padding-attack/#aes-in-electronic-code-book-mode)

**Code server**

    import sys, time
    from Crypto.Hash import SHA256
    from Crypto.Cipher.AES import AESCipher

    flag = "Flag goes here"

    def encrypt(m):
    key = SHA256.new(flag).digest()
    try:
        text = 'rowdy123' + m.decode('base64') + flag
    except:
        return "Opps, bad input"
    pad = 16 - (len(text) % 16)
    text += (chr(pad) * pad)
    cipher = AESCipher(key).encrypt(text).encode("base64")

    return cipher

    print "State your name (in base64): "
    m = raw_input()
    print "Here's the package:"
    print encrypt(m)


**Code exploit**


    from pwn import *

    def main(input):
        host = '34.74.132.34'
        port = 1337
        t = remote(host, port)
        t.recvline()
        input = base64.b64encode(input)
        t.sendline(input)
        a = t.recvall()
        encoded = "".join(a.split("\n")[1])
        hex = base64.b64decode(encoded).encode('hex')
        return hex[96:128]

    if __name__ == '__main__':
        flag = ""
        while True:

            """
            55 because it's:
            + 8 bytes for the initial block
            + 16*3 for the three next blocks
            - 1 byte to brute-force the character
            """

            payload = "0"*(55-len(flag))
            hex = main(payload)

            for i in range(33, 125):
                if main(payload + flag + chr(i)) == hex:
                    flag += chr(i)
                    print "Flag: ", flag
                    break

</details>


# AES MODE_CFB secret guessing

<details><summary>AES MODE_CFB</summary>



**Explication**

<details><summary>Explication</summary>

La méthode CFB chiffre d'abord iv puis XOR avec le texte en clair pour obtenir le texte chiffré, mais par défaut, seuls 8 bits sont utilisés, puis le segment de chiffrement suivant obtient 8 bits Si vous entrez directement le texte chiffré, vous pouvez obtenir le premier octet, puis changer le premier octet en texte brut pour obtenir le deuxième octet, mais comme l'hexagone n'est pas utilisé pour l'entrée à ce moment, il y aura des problèmes d'entrée d'octets. Donc, d'un autre point de vue, en analysant le texte en clair de k octets, puis en le comparant avec le texte chiffré k-ième octet, la même chose est correcte. C'est OK d'exploser un par un comme ça. Parce que je ne peux pas comprendre les images trouvées sur le site Web, j'ai perdu beaucoup de temps et je ne savais pas à quel point c'était facile jusqu'à ce que j'essaie un peu.

</details>

**Server Code**

    from Crypto.Cipher import AES
    from Crypto.Util.Padding import pad
    from os import urandom

    iv = urandom(16)
    key = urandom(16)
    FLAG = b"battleCTF{REDACTED}"

    def encrypt(data):
        cipher = AES.new(key, AES.MODE_CFB, iv)
        return cipher.encrypt(pad(data, 16))


    print(encrypt(FLAG).hex())
    while True:
        print(encrypt(input("> ").encode()).hex())



**Solution code**


    from pwn import *

    p = remote('chall.battlectf.online', 20001)
    #context. log_level = 'debug'

    enc = bytes.fromhex(p.recvline().decode().strip())[:-1]
    #v = enc
    flag = b'"
    while flag[-1:] != b'}":
        1 = len(flag)
        for i in range(0x20,0x7f):
        p.sendlineafter(b'>', flag + bytes([i]))
        v = bytes. fromhex(p.recvline().decode().strip())
        if v[fl] == enc[fl]:
        flag += bytes([i])
        print (flag)
        break



</details>
