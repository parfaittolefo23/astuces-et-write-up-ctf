# RSA small e attack
<details><summary>RSA small e attack</summary>

Check exploit here: [rsa_small_e_attack](https://gitlab.com/parfaittolefo23/astuces-et-write-up-ctf/-/blob/main/rsa_small_e_attack.py)

</details>

# ROCA vulnerability
<details><summary>ROCA vulnerability </summary>

Check on wikipedia to know more:

Solution:  [https://ctftime.org/writeup/8805](https://ctftime.org/writeup/8805)

Tool:

1. Use [_roca-detect_](https://github.com/crocs-muni/roca) to confirm the vulnérability or  [online tool](https://keychest.net/roca)

2.  Use [_neca_](https://gitlab.com/jix/neca) or [download here](https://gitlab.com/parfaittolefo23/astuces-et-write-up-ctf/-/blob/main/Tools/neca) to exploit the vulnerability 

</details>
