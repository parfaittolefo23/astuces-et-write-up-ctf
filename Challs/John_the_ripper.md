Ce 13/01/2022

Sources:

[Challenge tryhackme](https://tryhackme.com/room/linprivesc)
 
[Autres sources](https://www.cyberciti.biz/faq/unix-linux-password-cracking-john-the-ripper/)

[Cours ripper tryhackme](https://tryhackme.com/room/johntheripper0)
 
<details><summary>Cracker les mots de passe du systeme linux avec `unshadow` de john
</summary>



John the ripper est un outils de crackage de mot de passe. Il peut prendre en entrée un worldlist pour cracker un mot de passe. Pour les passwords linux, il est préférable d'utiliser les **fichier** `/etc/shadow` et `/etc/passwd`... Suivre les instructions suivantes:

`sudo /usr/sbin/unshadow /etc/passwd  /etc/shadow > /tmp/crack.password.db`  

`sudo unshadow /etc/passwd /etc/shadow > /tmp/crack.password.db` (La première commande est la même que celle ci)

`john /tmp/crack.password.db`

`john -show /tmp/crack.password.db`

</details>


<details><summary>Cracker password windows</summary>

Obtenir les hash avec hasdump metasploit


Copier toutes les hash ou la ligne voulu dans le fichier hash.txtx

Indiquer le chemin de votre worldlist

`john --wordlist=pass.txt --format=NT hashes.txt`

</details>



<details><summary>Ssh2john how to id_rsa</summary>

`/usr/share/john/ssh2john.py`

`/usr/share/john/ssh2john.py id_rsa > id_rsa.john`

`cat id_rsa.john`

`john --wordlist=/usr/share/wordlists/rockyou.txt id_rsa.john`


Source: [ssh2john-how-to](https://vk9-sec.com/ssh2john-how-to/)

</details>




