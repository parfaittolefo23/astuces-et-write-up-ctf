**Énumération de sous domaines**

<details><summary>OSINT - SSL/TLS Certificates</summary>

Une recherche sur les certificats des sites peut fournird'informations utiles.

Consultèe  [crt](https://crt.sh)  et [transparencyreport](https://transparencyreport.google.com/https/certificates)

Rechercher les sous domaines avec google sous cette forme.

-site:_www.tryhackme.com_  site:*._tryhackme.com_

</details>

<details><summary> DNS Bruteforce</summary>

**dnsrecon** est un outil spécialisé dans l'énumération DNS

` dnsrecon -t brt -d acmeitsupport.thm`

` dnsrecon -t brt -d site_address`
</details>

<details><summary> OSINT - Sublist3r</summary>

Consulter [Sublist3r](https://github.com/aboul3la/Sublist3r)

./sublist3r.py -d acmeitsupport.thm

./sublist3r.py -d subdomaine

</details>


<details><summary> Virtual Hosts</summary>

Ètant donné qu'il y a des site web qui seront hébergé sur des DNS non public, il faudrait fouillée plus loin. Les enrégistrement peuvent etre stocké dans:

`/etc/hosts file (or c:\windows\system32\drivers\etc\hosts file for Windows users)`

Fouille:  `ffuf -w /usr/share/wordlists/SecLists/Discovery/DNS/namelist.txt -H "Host: FUZZ.acmeitsupport.thm" -u http://10.10.37.238`

La commande ci-dessus utilise le commutateur -w pour spécifier la liste de mots que nous allons utiliser. Le commutateur -H ajoute/modifie un en-tête (dans ce cas, l'en-tête Host), nous avons le mot-clé FUZZ dans l'espace où un sous-domaine irait normalement, et c'est là que nous essaierons toutes les options de la liste de mots.


`ffuf -w /usr/share/wordlists/SecLists/Discovery/DNS/namelist.txt -H "Host: FUZZ.acmeitsupport.thm" -u http://10.10.37.238 -fs {size}`

-fs, indique à ffuf d'ignorer tous les résultats de la taille spécifiée


wwwmail                 [Status: 200, Size: 2395, Words: 503, Lines: 52]
ws11                    [Status: 200, Size: 2395, Words: 503, Lines: 52]
ye                      [Status: 200, Size: 2395, Words: 503, Lines: 52]
yellow                  [Status: 200, Size: 56, Words: 8, Lines: 1]
x                       [Status: 200, Size: 2395, Words: 503, Lines: 52]
young                   [Status: 200, Size: 2395, Words: 503, Lines: 52]
ws13                    [Status: 200, Size: 2395, Words: 503, Lines: 52]
x-ray                   [Status: 200, Size: 2395, Words: 503, Lines: 52]

Remaquez que le sous domaine dans ce lot est "yellow"

</details>
