**Outils et méthodologies de resolution**

- `Analyse visuelle`

- `file`

- `exiftool`

- `strings`

- `binwalk`

- `foremost`

- `scalpel`

- `fcrack`

- `steghide extract -sf image.png`

- `stegsolve`

- `stegcracker`

- `stegsnow`

- `TweakPNG`

- `pngcheck -vtp7 image.png`

- `ghex`

- `xxd`

- `stegdetect`

**Reverse image search**

- `tineye`

**Online tools**

- Equivalent stegsolve Online

[stegoline.georgeom](https://stegonline.georgeom.net/upload)

- ELA (Error Level Analysis)

[fotoforensics](http://fotoforensics.com/)

- FFT Analysis (Fast Fourier Transform)

[bigwww.epfl](http://bigwww.epfl.ch/demo/ip/demos/FFT/)

**Outils classiques**

- `steganabra`

**Outils moins courant**

- `stegseek`

- `OpenStego`

- `Stegpy`

- `Outguess`

- `jphide`

- `jpgx`


**Audio Analyse**

- `Audacity`

- `MixW`

- `MMSSTV`

- `RX SSTV`

- `Sonic visualiser`


**Video Analyse**

- `Sonic visualiser`

- `ffmpg`

- `vlc`

- `veracrypt`



<details><summary>Autres sources</summary>

On line: [Tools 1 ](https://0xrick.github.io/lists/stego/#steganography)

On line: [Tools 2](https://githubhelp.com/search?type=repos&q=Zlib)

https://infosecwriteups.com/beginners-ctf-guide-finding-hidden-data-in-images-e3be9e34ae0d

</details>

