
**Veuillez toujours mettre à jour ce fichier lorsque vous ajoutez un fichier dans ce dossier, pour s'assurer que sa disparition n'est pas du à une suppremssion accidentel par le push de quelqu'un.
Si un fichier a été supprimé intentionnellement, veuillez ne pas supprimer sa ligne dans ce fichier mais simplement le marquer comme supprimé et ajouter la raison de la suppression**
 

<details><summary>Modèle d'écriture dans ce fichier</summary>

Répertoire/Fichier ( Si_Supprimé* )

> url -- date


Description explicite et utilisation :

Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint exercitationem expedita error nobis minus maiores molestiae saepe laboriosam non modi nihil, voluptatibus dicta ullam, aperiam, veniam possimus dolores! Eaque, veritatis!




( Raison(s)_Si_Supprimé )




Ex :

Scripts/Bash/Reverse_tool_1.sh ( Supprimé )


https://le-hacker.fr/reverse_tool.sh -- 02/01/2022 14:45:25
Description explicite et utilisation :

Ce script permet de prendre le contrôle du machine distante par ssh en exploitant une vulnérabilité du protocole http dans sa version 2.4.
Pour l'utiliser , ........


Il a été remplacé par Scripts/Bash/Reverse_tool_2.sh qui exploite plus vulnérabilité et est plus à jour

</details>

