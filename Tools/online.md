<details><summary>Crack password</summary>

Online crack password: [d00mfist.gitbooks.io](https://d00mfist.gitbooks.io/ctf/content/online_password_cracking.html)

Online crack password: [crackstation](https://crackstation.net/)

</details>

<details><summary>Payloads</summary>

[Payloads metasploit](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md)

</details>

<details><summary> Finding Manual Exploits </summary>

[Exploit-db](exploit-db.com/)

[Exploit rapid7](https://www.rapid7.com/db/)


[NVD CVE](https://nvd.nist.gov/vuln/detail)

`Searchsploit`  en ligne de commande linux
</details>


<details><summary>Nmap scanning</summary>

scannage avec adresse Usupée (IP ET MAC)

`nmap -S SPOOFED_IP MACHINE_IP`

`--spoof-mac SPOOFED_MAC`


Scannage avec Ip isupée recommandée 

`nmap -e NET_INTERFACE -Pn -S SPOOFED_IP MACHINE_IP`


Scannage avec plusieurs machines sources (ip sources)

`nmap -D 10.10.0.1,10.10.0.2,RND,RND,ME MACHINE_IP`

Les RND sont données par randome et ME sera remplacée par mon IP


Nmap fournit l'option `-f` pour fragmenter les paquets. Une fois choisies, les données IP seront divisées en 8 octets ou moins. L'ajout d'un autre -f (-f -f ou `-ff)` divisera les données en fragments de 16 octets au lieu de 8. Vous pouvez modifier la valeur par défaut en utilisant --mtu ; cependant, vous devez toujours choisir un multiple de 8.

Scannage Zombie

`nmap -sI ZOMBIE_IP MACHINE_IP`

</details>


<details><summary>Criptographie</summary>

[Cypher identifier](https://www.boxentriq.com/code-breaking/cipher-identifier)

[Decoder -----------........---------](http://www.unit-conversion.info/texttools/morse-code/)


[Compilateur >>---@ Befunge-93](https://esolangpark.vercel.app/ide/befunge93)


[Compilateur >>---@ Befunge](https://esolang.rutteric.com/fungejs/?initprog=JTNFJTIwJTIwJTIwJTIwJTIwJTIwJTIwJTIwJTIwJTIwJTIwJTIwJTIwNy0wNTUlMkJwOTUlMkItMTE5JTJCcDk0ODY3JTJCJTJCJTJCJTJCLTI3NCUyQjEtcDc3ODM1MTEzNyUyQiUyQiUyQiUyQiUyQiUyQiUyQiUyQiUyQnYlMEElMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjB2MTk0LS0xLTQtOC02JTJCJTJCJTJCMzUzOXAtMiUyQjM5MyUyMCUyMCUzQyUwQXYxNiUyQiUyQjc0cCUyQjU1NSUyQiUyQiUyQiUyQiUyQiUyQiUyQiUyQiUyQiUyQiUyQjk5NDg1MjcxNTM2cCUyQiUzQyUwQSUzRTklMkJwdiUwQSUyMCUyMCUyMCUyMCUzRTQxLTkyJTJCJTJCLTc0NSUyQjMtNCUyQnA3Mi04ODglMkIlMkItNzU5MSUyQiUyQiUyQiUyQiUyQjg5MjQlMkIlMkI1LXA1NTU1NDEtJTJCJTJCJTJCJTJCdiUwQXYlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlMjAlM0MlMEElM0UlMkI0NSUyQjc3MjQlMkIlMkIlMkI1LTUtcDc5JTJCNDUlMkItNTIlMkItJTJCMzExNSUyQiUyQiUyQjY3JTJCMy1wNjYlMkItMTE0JTJCJTJCNyUyQjItNDQ0Mi0lMkIlMkJwdiUwQSUwQSUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCUyMCU0MHAtMSUyQjU2JTJCJTJCJTJCJTJCJTJCJTJCJTJCJTJCJTJCJTJCJTJCMTExMTExMTExMTExLSUyQiUyQiUyQiUyQiUyQjU2NzczNCUzQyUwQSUwQXYlMkJMJTVFMGslNDAlMjV%2BfmElNjBG)

[dogmamix any algo](https://dogmamix.com/MimeHeadersDecoder/)

[quipqiup](https://www.quipqiup.com/)  pour les substitutions et autres

Decoder les message secretes Twiter: [Twiter cipher decoder](https://holloway.nz/steg/)

</details>

<details><summary>Stegano</summary>

On line: [Tools 1 ](https://0xrick.github.io/lists/stego/#steganography)

On line: [Tools 2](https://githubhelp.com/search?type=repos&q=Zlib)

</details>

<details><summary>Ctf tools and writeup</summary>

Directives, writeup: [0xrick.github.io](https://0xrick.github.io/hack-the-box/dab/)

</details>

<details><summary>Injection commande</summary>

Essayez d'injecter les commandes en les passant en paramètre si des variables sont prises en paramètre.
Pour des serveurs utilisant le python, mettez la commande suite au /

Voici un payload de test: `curl http://vulnerable.app/process.php%3Fsearch%3DThe%20Beatles%3B%20whoami`

`timeout`  est un bon payload de test pour windows, n'oublier pas `ping`

Payloads [github](https://github.com/payloadbox/command-injection-payload-list)

</details>

<details><summary>FTP</summary>

`STAT`  peut fournir des informations supplémentaires

`SYST`   affiche le type de système de la cible (UNIX dans ce cas)

`PASV`  passe le mode en passif

`TYPE A` bascule le mode de transfert de fichiers en ASCII

Le mode utilisé pour le transfert des fichiers text, image ou autre.

`TYPE I` bascule le mode de transfert de fichiers en binaire

Les deux modes:

Actif : En mode actif, les données sont envoyées sur un canal séparé provenant du port 20 du serveur FTP .

Passif : En mode passif, les données sont envoyées sur un canal séparé provenant du port d'un client FTP au-dessus du numéro de port 1023.

</details>

<details><summary>POP</summary>

L'authentification est requise pour accéder aux messages électroniques

l'utilisateur s'authentifie en fournissant son identifiant `USER frank` 

et son mot de passe `PASS D2xc9CgD`. En utilisant la commande `STAT`,

nous obtenons la réponse +OK 1 179; basé sur RFC 1939 , une réponse

positive à `STAT` a le format `+OK nn mm`, où _nn est le nombre de

messages électroniques dans la boîte de réception et mm est la taille

de la boîte de réception en octets (octet)_. La commande `LIST` a

fourni une liste de nouveaux messages sur le serveur, et `RETR 1`

</details>

<details><summary>Attaque de l'homme du milieu (MITM)</summary>

Plusieurs outils existent pour la réalisation de cette attaque...

Nous avons [ettercap](https://www.ettercap-project.org/) et [bettercap](https://www.bettercap.org/) en principal

</details>

<details><summary>scp</summary>

`scp mark@10.10.93.35:/home/mark/archive.tar.gz -j .` 

Cette commande copiera un fichier nommé `archive.tar.gz` du système distant situé dans le `/home/mark` répertoire vers . ( le repetoire actuel)

`scp backup.tar.bz2 mark@10.10.93.35:/home/mark/`

Cette commande copiera le fichier `backup.tar.bz2` du système local vers le répertoire  `/home/mark/` du système distant.

</details>

All tools of ctf: [ctf-checklist-for-beginner](https://fareedfauzi.gitbook.io/ctf-checklist-for-beginner/cryptography)

Lire fichier .zlib

`python -c "import zlib; f=open('flag.zlib','rb').read();print(zlib.decompress(f))"`

lister les tables d'une bade de donnée

    SELECT table_name
    FROM information_schema.tables
    WHERE table_schema = 'public'
    ORDER BY table_name;


<details><summary>Click to expand</summary>

Binary shellcode  --> [picoctf-2018-guide-shellcode](https://www.nullhardware.com/reference/hacking-101/picoctf-2018-binary-exploits/shellcode/)

</details>
