# Utilités de forensic

<details><summary>Discover</summary>

* Les bloqueur [ici](https://www.forensicswiki.org/wiki/Write_Blockers)

Les bloqueurs en écriture sont donc des dispositifs qui permettent de faire l’acquisition d’une image d’un disque dur en bloquant le mécanisme d’écriture, mais pas de lecture

* sleuthkit [ici](https://www.sleuthkit.org/) (Autopsy) analyse de disque dur

Le framework The Sleuth Kit permet de réaliser une analyse forensic en passant de la génération d’une timeline au triage des données et à l’analyse des artefacts Windows (registre, email, historique…), jusqu’à la génération d’un rapport. Il comporte une interface graphique appelée Autopsy.

* [ici](https://www.nirsoft.net/) ou [sysinternals](https://docs.microsoft.com/en-us/sysinternals/downloads/) de microsoft

* Framework de google [ici](https://github.com/google/grr)

* Les utilitaires de linux par defaut

Les [SIFT](https://digital-forensics.sans.org/community/downloads) gratuits

Les [tsurugi](https://tsurugi-linux.org/)

Le [caine](https://www.caine-live.net/)

Le [deftlinux](http://www.deftlinux.net/)

* Pour l’analyse de la mémoire vive, il existe également des frameworks tels que [Volatility](https://www.volatilityfoundation.org/) et [Rekall](http://www.rekall-forensic.com/).

* Pour la copie des disques durs et de la RAM, il est possible d’utiliser le freeware [FTK Imager Lite](http://marketing.accessdata.com/ftkimagerlite3.1.1).
</details>

# Essentiel commandes volatility & others

<details><summary>Discover</summary>


# What type of dump am I going to analyze ?

 volatility -f MyDump.dmp imageinfo

# Which process are running

 volatility -f MyDump.dmp --profile=MyProfile pslist

 volatility -f MyDump.dmp --profile=MyProfile pstree

 volatility -f MyDump.dmp --profile=MyProfile psxview

# List open TCP/UDP connections

 volatility -f MyDump.dmp --profile=MyProfile connscan

 volatility -f MyDump.dmp --profile=MyProfile sockets

 volatility -f MyDump.dmp --profile=MyProfile netscan

 volatility -f MyDump.dmp --profile=MyProfile connections

# What commands were lastly run on the computer

 volatility -f MyDump.dmp --profile=MyProfile cmdline

 volatility -f MyDump.dmp --profile=MyProfile consoles

 volatility -f MyDump.dmp --profile=MyProfile cmdscan

# Dump processes exe and memory 

 volatility -f MyDump.dmp --profile=MyProfile procdump -p MyPid --dump-dir .

 volatility -f MyDump.dmp --profile=MyProfile memdump -p MyPid --dump-dir .

# Hive and Registry key values

 volatility -f MyDump.dmp --profile=MyProfile hivelist

 volatility -f MyDump.dmp --profile=MyProfile printkey -K "MyPath"

# Use plugin to specifique process

 volatility -f MyDump.dmp --profile=MyProfile envars --pid mtpid

# Get all informations about a process

 volatility -f MyDump.dmp --plugin=/usr/share/volatility/plugins/ --profile=MyProfile psinfo -p mtpid

# Get  informations about a process and his dll or dump dll

 volatility -f ch2.dmp --profile=Win7SP0x86 dlllist -p 3144

 volatility -f MEMORY_FILE.raw --profile=PROFILE --pid=PID dlldump -D <Destination Directory>

# Détectez l'injection de code avec malfind

 volatility -f memdump.mem --profile=Win7SP1x86 malfind [-p 1254] -D <Destination Directory>

 volatility -f memdump.mem --profile=Win7SP1x86apihooks

# Detectez les processus cachés

Les tois colonnes seront à false dans le cas 

 volatility -f memdump.mem --profile=Win7SP1x86 ldrmodules

# Listez les mutex avec mutantscan

 volatility -f memdump.mem --profile=Win7SP1x86 mutantscan 

# Extraction des services

 volatility -f memdump.mem –-profile=Win7SP1x86 svcscan

# YARA rules

 volatility -f memdump.mem --profile=Win7SP1x86 yarascan

Les YARA rules sont un moyen de chercher des caractéristiques particulières des malwares dans des fichiers, afin de détecter des fichiers malveillants

# Les plugins Volatility

 volatility -f memdump.mem --profile=Win7SP1x86 --plugins=“plugin_dir”

Nous pouvons voir ici un exemple avec le plugin **autoruns**, il vous permet de détecter quelles sont les manières dont le malware persiste sur la machine.

Consultez [ici](https://www.forensicswiki.org/wiki/List_of_Volatility_Plugins) une liste de plugin

# Recuperer toutes les données sur un disque dur

 testdisk

# Hiberfil.sys file

Hiberfil.sys est le fichier utilisé par défaut par Windows pour enregistrer l'état de la machine dans le cadre du processus d'hibernation.

Le fichier hiberfil.sys est par défaut compressé ; pour pouvoir l'analyser avec Volatility, il faudra d’abord utiliser l’option imagecopy pour décompresser l’image. 

 `volatility -f hiberfil.sys -–profile=Win7SP1x64 imagecopy -O hiberfil.dmp`

# Pagefile.sys

La pagination est un concept qui permet d’étendre la mémoire RAM disponible en stockant dans un fichier des éléments de la RAM qui ne sont pas utilisés. Windows utilise le fichier système Pagefile.sys pour stocker ces informations, qui peuvent également être exploitées durant l’investigation. 

 `strings pagefile.sys | grep “http://”`

# Veifier les infos sur les fichiers dll ou virus dumpés ici

Une analyse totatl des virus    [VirusTotal](https://www.virustotal.com/gui/home/upload)

Pour une analyse hybride:       [Hybrid Analysis](https://www.hybrid-analysis.com/)

# Retrouver la localisation d'imageinfo

 [exif.regex.info](http://exif.regex.info/exif.cgi)
 

 [www.pic2map]( https://www.pic2map.com/)
 

# Forensic image 

 [fotoforensics](http://fotoforensics.com/)



</details>

# Online analyse tools

<details><summary>Discover</summary>



[virustotal.com](https://www.virustotal.com/gui/file/5b136147911b041f0126ce82dfd24c4e2c79553b65d3240ecea2dcab4452dcb5/detection)


[www.hybrid-analysis.com](https://www.hybrid-analysis.com/sample/5b136147911b041f0126ce82dfd24c4e2c79553b65d3240ecea2dcab4452dcb5/)


Explication des fichiers [BMP](http://www.ece.ualberta.ca/~elliott/ee552/studentAppNotes/2003_w/misc/bmp_file_format/bmp_file_format.htm)

Lecture [qr code](https://zxing.org/w/decode.jspx)
</details>

# Récupérez les informations de base du disque pour l'analyse

<details><summary>Discover</summary>

# Comment sont stockées les données sur un système Windows ?

<details><summary>Discover</summary>

**Organisation des données sur un volume NTFS**

Un volume NTFS est organisé de la manière suivante :

**NTFS Boot Sector** : contient le bloc de paramètres BIOS qui stocke des informations sur la structure du volume et les structures du système de fichiers, ainsi que le code de démarrage qui charge Windows ;

**Master File Table** : contient les informations nécessaires pour extraire des fichiers de la partition NTFS, tels que les attributs d'un fichier ;

**File System Data** : stocke les données qui ne sont pas contenues dans la MFT ;

**Master File Table Copy** : inclut des copies des enregistrements indispensables à la restauration du système de fichiers en cas de problème avec la copie originale.

# Extraction de la MFT pour l'analyser

Le tableau suivant présente les fichiers système présents à la racine.

| Entrée | Filename |Description |
---------|----------|------------|
|0 $MFT |Il contient tous les enregistrements des fichiers stockés et leurs informations (nom, horodatage, type de fichier, etc.)|
|1 |$MFTMirr |Cette partie est une copie des 4 premières entrées MFT utilisées pour restaurer une partition.|
|2 |$LogFile |Il s'agit d'un fichier journal contenant toutes les actions effectuées sur le volume. |
|3 |$Volume |Il contient toutes les informations du volume, telles que le nom ou la version du système de fichiers. |
|4 |$AttrDef |Il contient la liste de tous les attributs définis par le système sur le volume |
| 5 |.|Le répertoire racine. |
|6 |$Bitmap |Cette partie représente les clusters free ou inutilisés sur le volume |
| 7 |$Boot |Cette partie contient les informations de Boot. |
|8 |$BadClus |Cette partie contient tous les clusters ayant des secteurs défectueux sur le volume. Cela permet également d'identifier les clusters non référencés |
|9 |$Secure | Contient des descripteurs de sécurité uniques pour tous les fichiers d'un volume |
| 10 |$UpCase |Il contient une table de caractères Unicode en majuscules pour effectuer la correspondance sous Windows et DOS|
|11 |$Extend |Utilisé pour diverses extensions facultatives telles que $Quota, $ObjId, $Reparse |


# Extrayez la MFT et exploiter MFT

Voir [openclassrooms](https://openclassrooms.com/fr/courses/1750151-menez-une-investigation-d-incident-numerique-forensic/6474105-recuperez-les-informations-de-base-du-disque-pour-lanalyse#/id/r-6534184) pour extraire la MFT.

Voir [Comment convertir](https://openclassrooms.com/fr/courses/1750151-menez-une-investigation-d-incident-numerique-forensic/6474105-recuperez-les-informations-de-base-du-disque-pour-lanalyse#/id/r-6474067) MFT EN CSV


</details>

# Unicode Steganography with Zero-Width Characters

[Unicode Steganography with Zero-Width](https://330k.github.io/misc_tools/unicode_steganography.html)

</details>
Root-me Forensic solutions: [solutions Forensic](https://digitalitskills.com/root-me-command-and-control-challenges/)
