<details><summary>Advenced scanning</summary>

scannage avec adresse Usupée (IP ET MAC)

`nmap -S SPOOFED_IP MACHINE_IP`

`--spoof-mac SPOOFED_MAC`


Scannage avec Ip isupée recommandée 

`nmap -e NET_INTERFACE -Pn -S SPOOFED_IP MACHINE_IP`


Scannage avec plusieurs machines sources (ip sources)

`nmap -D 10.10.0.1,10.10.0.2,RND,RND,ME MACHINE_IP`

Les RND sont données par randome et ME sera remplacée par mon IP



Null Scan TCP	sudo nmap -sN 10.10.154.197

Analyse FIN TCP	sudo nmap -sF 10.10.154.197

Analyse de Noël TCP	sudo nmap -sX 10.10.154.197

Analyse Maimon TCP	sudo nmap -sM 10.10.154.197

Balayage TCP ACK	sudo nmap -sA 10.10.154.197

Balayage de la fenêtre TCP	sudo nmap -sW 10.10.154.197

Analyse TCP personnalisée	sudo nmap --scanflags URGACKPSHRSTSYNFIN 10.10.154.197

IP source usurpée	sudo nmap -S SPOOFED_IP 10.10.154.197

Adresse MAC usurpée	--spoof-mac SPOOFED_MAC

Balayage leurre	nmap -D DECOY_IP,ME 10.10.154.197

Balayage inactif (Zombie)	sudo nmap -sI ZOMBIE_IP 10.10.154.197

Fragmenter les données IP en 8 octets	-f

Fragmentez les données IP en 16 octets	-ff

</details>


<details><summary>Nmap script</summary>

`auth`	Scripts liés à l'authentification

`broadcast`	Découvrez les hôtes en envoyant des messages de diffusion

`brute`	Effectue un audit de mot de passe par force brute contre les connexions

`default`	Scripts par défaut, identiques à-sC

`discovery`	Récupérer des informations accessibles, telles que des tables de base de données et des noms DNS

`dos`	Détecte les serveurs vulnérables au déni de service ( DoS )

`exploit`	Tentatives d'exploitation de divers services vulnérables

`external`	Vérifications à l'aide d'un service tiers, tel que Geoplugin et Virustotal

`fuzzer`	Lancer des attaques fuzzing

`intrusive`	Scripts intrusifs tels que les attaques par force brute et l'exploitation

`malware`	Analyse les portes dérobées

`safe`	Des scripts sûrs qui ne planteront pas la cible

`version`	Récupérer les versions de service

`vuln`	Vérifie les vulnérabilités ou exploite les services vulnérables


...

</details>


<details><summary>Options</summary>


`-sV`	determine service/version info on open ports

`-sV` --version-light	try the most likely probes (2)

`-sV `--version-all	try all available probes (9)

`-O`	detect OS

`--traceroute`	run traceroute to target

`--script=`"SCRIPTS"	Nmap scripts to run

`-sC` or `--script=default`	run default scripts

`-A`	equivalent to -sV -O -sC --traceroute

`-oN`	save output in normal format

`-oG`	save output in grepable format

`-oX`	save output in XML format

-`oA`	save output in normal, XML and Grepable formats
<details><summary>Hydra</summary>


Consulter ici [Hydra github](https://github.com/frizb/Hydra-Cheatsheet)
</details>
</details>
