Sources de tools de OSINT:  [www.aware-online.com/en/osint-tutorials](https://www.aware-online.com/en/osint-tools/photo-and-video-tools)


<details><summary>OSINT IMAGES</summary>

# Recherche, reverse images

[yandex.com/images](https://yandex.com/images/)

# Recherche avec google images 

[images.google.com](https://images.google.com/?gws_rd=ssl)

# Comparer deux images 

[www.diffchecker.com](https://www.diffchecker.com/image-diff/)

# Retrouver la localisation d'image
 [exif.regex.info](http://exif.regex.info/exif.cgi)

 [pic2map.com](https://www.pic2map.com/npusjj.html[](url))

# Retrouver la localisation d'image avec latitude/longitude GPS

 [maps.ie/coordinates](https://www.maps.ie/coordinates.html)

# All hidden data

[aperisolve online](https://aperisolve.fr/d3a9a07b754db9dad26bfb235ae95f3a)
</details>
